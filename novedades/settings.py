# coding=utf-8
"""
Django settings for novedades project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys  

reload(sys)  
sys.setdefaultencoding('utf8')

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'bdj9&x673geor-%j)!wr-&#zjxpm(=qd&!v)3g08a6o&*um$n+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'novedades.apps.comun',
    'novedades.apps.usuario',
    'novedades.apps.novedad',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.locale.LocaleMiddleware'
)

ROOT_URLCONF = 'novedades.urls'

WSGI_APPLICATION = 'novedades.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        #'ENGINE': 'django.db.backends.sqlite3',
        #'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'novedades',
        'USER': 'admin',
        'PASSWORD': '123456',
        'HOST': 'localhost',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'es-ve'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_ROOT = ''
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'novedades/static/'),
)
STATICFILES_FINDER = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

BASE_TEMPLATES = os.path.join(BASE_DIR, "novedades/templates")

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_TEMPLATES],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

## URL de acceso al sistema
LOGIN_URL = "/login/"
## URL de salida del sistema
LOGOUT_URL = "/logout/"

## Servidor de correo saliente
EMAIL_HOST = 'smtp.gmail.com'

## Puerto del servidor de correo
EMAIL_PORT = 25

## Usuario de correo
EMAIL_HOST_USER = 'rvargas.pruebas@gmail.com'

## Contraseña de correo
EMAIL_HOST_PASSWORD = 'f@vj/wnd'

## Protocolo de seguridad de la conexión al servidor de correo
EMAIL_USE_TLS = True

## Dirección de correo de quién envia
EMAIL_FROM = 'incidencias@bomberosmerida.gob.ve'

## Expiración de la sesión si el navegador web es cerrado (no funciona con google chrome)
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

## Tiempo de expiración dela sesión
#SESSION_COOKIE_AGE = 5 * 60