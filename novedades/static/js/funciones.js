function actualizar_combo(url, opcion, combo, app, mod, campo, n_value, n_text, bd) {
    $.ajaxSetup({
        async: false
    });
    $.getJSON(url, {opcion:opcion, combo:combo, app:app, mod:mod, campo:campo, n_value:n_value,
        n_text: n_text, bd:bd}, function(datos) {
        if (datos.resultado) {

            if (datos.combo_disabled == "false") {
                $("#"+combo).removeAttr("disabled");
            }
            else {
                $("#"+combo).attr("disabled", "true");
            }

            $("#"+combo).html(datos.combo_html);
        }
        else {
            bootbox.alert(datos.error);
        }
    }).fail(function(jqxhr, textStatus, error) {
         var err = textStatus + ", " + error;
         bootbox.alert( "Petición fallida. Verifique el error: " + err );
    });
}

function buscar_persona(url, cedula) {
    $.getJSON(url, {cedula: cedula}, function(datos) {
        if (datos.resultado) {
            $("#id_persona_nombre").val(datos.nombre);
            /* Asigna el valor del combo sexo y selecciona el mismo para que se muestre */
            $("#id_persona_sexo").select2('val', datos.sexo).change();
            $("#id_persona_edad").val(datos.edad);
        }
        else {
            bootbox.alert("Registro no encontrado")
        }
    }).fail(function(jqxhr, textStatus, error) {
         var err = textStatus + ", " + error;
         bootbox.alert( "Petición fallida. Verifique el error: " + err );
    });
}

function eliminar_registro(url, label, modelo, id) {
    bootbox.confirm("Esta seguro de querer eliminar el registro seleccionado?", function(result) {
        if (result) {
            $.getJSON(url, {app_label: label, modelo: modelo, id: id}, function (datos) {
                var msg = "";
                if (datos.resultado) {
                    msg = "Registro eliminado con éxito";
                }
                else {
                    msg = "No se puede eliminar el registro seleccionado";
                }

                bootbox.alert(msg, function() {
                    window.location = window.location.href;
                });
            });
        }
    });
}

function validar_solo_numeros(evt) {
    evt = (evt) ? evt:event;
    var tecla = (evt.which) ? evt.which:evt.keyCode;
    var keychar = String.fromCharCode(tecla);

    return !!/^([0-9\t\b])$/.test(keychar);
}

function validar_numeros(evt) {
    evt = (evt) ? evt:event;
    var tecla = (evt.which) ? evt.which:evt.keyCode;
    var keychar = String.fromCharCode(tecla);

    return !!/^([0-9\t\b]|\/|\.|-)$/.test(keychar);
}

function validar_horas(evt) {
    evt = (evt) ? evt:event;
    var tecla = (evt.which) ? evt.which:evt.keyCode;
    var keychar = String.fromCharCode(tecla);

    return !!/^([0-9\t\b]|\:)$/.test(keychar);
}

function validar_letras(evt) {
    evt = (evt) ? evt:event;
    var tecla = (evt.which) ? evt.which:evt.keyCode;
    var keychar = String.fromCharCode(tecla);

    return !!/^([A-Za-z\s\b]|\.|\(|\)|ñ|Ñ)$/.test(keychar);
}

function validar_formulario_novedad(id_procedimiento) {
    
    var procedimiento = [
        {"pk": 1, "nombre": "Incendio de Estructuras", 'mostrar': ['estructura', 'comision', 'persona']},
        {"pk": 2, "nombre": "Incendio de Vehículo", 'mostrar': ['persona', 'vehiculo', 'comision']},
        {"pk": 3, "nombre": "Incendios Forestales", 'mostrar': ['persona', 'comision']},
        {"pk": 4, "nombre": "Incendios de Cultivos", 'mostrar': ['persona', 'comision']},
        {"pk": 5, "nombre": "Incendios de Vegetación / Maleza", 'mostrar': ['comision']},
        {"pk": 6, "nombre": "Escape de gas licuado del petróleo", 'mostrar': ['persona', 'comision']},
        {"pk": 7, "nombre": "Explosiones / Defragación", 'mostrar': ['persona', 'comision']},
        {"pk": 8, "nombre": "Cortos circuítos", 'mostrar': ['persona', 'comision']},
        {"pk": 9, "nombre": "Quemas de basura", 'mostrar': ['comision']},
        {"pk": 10, "nombre": "Quemas de neumáticos", 'mostrar': ['comision']},
        {"pk": 11, "nombre": "Control / Neutralización de combustible", 'mostrar': ['persona', 'comision']},
        {"pk": 12, "nombre": "Control / Neutralización de materiales peligrosos", 'mostrar': ['persona', 'comision']},
        {"pk": 13, "nombre": "Servicios de abastecimiento de agua", 'mostrar': []},
        {"pk": 14, "nombre": "Control / Exterminio de plagas/insectos", 'mostrar': ['persona', 'comision']},
        {"pk": 15, "nombre": "Desbordamiento de Río", 'mostrar': ['persona', 'comision']},
        {"pk": 16, "nombre": "Deslizamiento de tierra", 'mostrar': ['persona', 'comision']},
        {"pk": 17, "nombre": "Recuperación de Cadáver", 'mostrar': ['persona', 'comision']},
        {"pk": 18, "nombre": "Rescate de animales", 'mostrar': ['persona', 'comision']},
        {"pk": 19, "nombre": "Rescate de llaves", 'mostrar': ['persona']},
        {"pk": 20, "nombre": "Rescate de personas en asensor", 'mostrar': ['persona', 'comision']},
        {"pk": 21, "nombre": "Rescate de personas incomunicadas", 'mostrar': ['persona', 'comision']},
        {"pk": 22, "nombre": "Rescates en aguas", 'mostrar': ['persona', 'comision']},
        {"pk": 23, "nombre": "Búsqueda y rescate en montañas", 'mostrar': ['persona', 'comision']},
        {"pk": 24, "nombre": "Rescates en estructuras colapsadas", 'mostrar': ['persona', 'comision']},
        {"pk": 25, "nombre": "Accidentes aéreos", 'mostrar': ['persona', 'comision']},
        {"pk": 26, "nombre": "Talas / Podas de árboles", 'mostrar': ['persona', 'comision']},
        {"pk": 27, "nombre": "Intentos de suicidio", 'mostrar': ['persona', 'comision']},
        {"pk": 28, "nombre": "Inundaciones de viviendas", 'mostrar': ['persona', 'comision', 'estructura']},
        {"pk": 29, "nombre": "Viviendas afectadas", 'mostrar': ['persona', 'comision', 'estructura']},
        {"pk": 30, "nombre": "Personas afectadas por lluvias", 'mostrar': ['persona', 'comision']},
        {"pk": 31, "nombre": "Accidente de tránsito", 'mostrar': ['persona', 'comision', 'vehiculo']},
        {"pk": 32, "nombre": "Servicios médicos pre-hospitalarios", 'mostrar': ['persona']},
        {"pk": 33, "nombre": "Servicios de Ambulancia", 'mostrar': ['persona']},
        {"pk": 34, "nombre": "Atenciones primarias", 'mostrar': ['persona']},
        {"pk": 35, "nombre": "Acto de presencia en persona fallecida", 'mostrar': ['persona', 'comision']},
        {"pk": 36, "nombre": "Guardias de Prevención", 'mostrar': []}
    ];

    $("#line-persona").hide();
    $(".persona").hide();
    $("#line-comision").hide();
    $(".comision").hide();
    $("#line-vehiculo").hide();
    $(".vehiculo").hide();
    $("#line-estructura").hide();
    $(".estructura").hide();

    for (i=0; i<procedimiento.length; i++) {
        for (x=0; x<procedimiento[i].mostrar.length; x++) {
            if (id_procedimiento==procedimiento[i].pk) {
                $("#line-"+procedimiento[i].mostrar[x]).show();
                $("."+procedimiento[i].mostrar[x]).show();
            }
        }
    }
    return true;
}

/* http://stackoverflow.com/questions/22172604/convert-image-url-to-base64 */
function getBase64Image(img) {
    alert(img);
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

function imprimir_reporte(cintillo) {
    /* configuración del cintillo */
    var img = document.getElementById("cintillo");
    var c = document.getElementById("myCanvas");
    c.width = img.width;
    c.height = img.height;
    var ctx = c.getContext("2d");
    ctx.drawImage(img, 0, 0);

    var g = document.getElementById("grafico-reporte");
    /*var gtx = g.getContext("2d");
    var imgData=gtx.getImageData(10,10,50,50);
    gtx.putImageData(imgData, 10, 70);*/
    
    var doc = new jsPDF("l", "pt", "letter");
    doc.setFont("arial", "bold");
    
    /* título del reporte */
    doc.setTextColor(4,6,133);
    doc.setFontSize(18);
    doc.text(250, 80, 'Dirección de Bomberos del Estado Mérida');
    doc.text(300, 100, 'Reporte General Año ' + $("#id_anho").val());
    /* Gráfico de estadísticas */
    doc.addImage(g.toDataURL(), 10, 160);

    /* Leyenda */
    doc.setTextColor(0,0,0);
    doc.setFontSize(10);
    doc.setDrawColor(0);
    doc.setFillColor(151,187,205);
    doc.roundedRect(540, 180, 10, 10, 3, 3, 'FD');
    doc.text(570, 190, 'Actividades Especiales');
    doc.setFillColor(222,63,66);
    doc.roundedRect(540, 210, 10, 10, 3, 3, 'FD');
    doc.text(570, 220, 'Búsqueda y Rescate');
    doc.setFillColor(253,180,92);
    doc.roundedRect(540, 240, 10, 10, 3, 3, 'FD');
    doc.text(570, 250, 'Combate de Incendios');
    doc.setFillColor(70,191,189);
    doc.roundedRect(540, 270, 10, 10, 3, 3, 'FD');
    doc.text(570, 280, 'Emergencias Pre-Hospitalarias');

    /* Tabla de datos estadísticos */
    var datos_ae = ["Actividades Especiales"], datos_br = ["Búsqueda y Rescate"], 
        datos_ci = ["Combate de Incendios"], datos_ep = ["Emergencias Pre-Hospitalarias"];

    var total_ae = 0, total_br = 0, total_ci = 0, total_ep = 0;
    for (i=0; i<12; i++) {
        datos_ae.push($("#ae-"+i).html());
        datos_br.push($("#br-"+i).html());
        datos_ci.push($("#ci-"+i).html());
        datos_ep.push($("#ep-"+i).html());
        total_ae += parseInt($("#ae-"+i).html());
        total_br += parseInt($("#br-"+i).html());
        total_ci += parseInt($("#ci-"+i).html());
        total_ep += parseInt($("#ep-"+i).html());
    }
    
    datos_ae.push(total_ae);
    datos_br.push(total_br);
    datos_ci.push(total_ci);
    datos_ep.push(total_ep);
    var columns = [
        "División", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre", "Total"
    ];

    var rows = [datos_ae, datos_br, datos_ci, datos_ep];
    doc.autoTable(columns, rows, {
        theme: 'plain',
        tableWidth: 'wrap',
        headerStyles: {halign: 'center'},
        margin: {top: 360, left:15},
        /*bodyStyles: {
            fillColor: [52, 73, 94],
            textColor: 240
        },
        alternateRowStyles: {
            fillColor: [74, 96, 117]
        },*/
        beforePageContent: function(data) {
            /* encabezado del reporte */
            doc.addImage(c.toDataURL(), 10, 10, img.width - 260, img.height-40);
        },
        afterPageContent: function(data) {
            doc.text(300, 595, "Dirección de Bomberos del Estado Mérida");
        }
    });
    doc.output('save');
    //doc.output('dataurlnewwindow');
}