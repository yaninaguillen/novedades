# coding=utf-8
from django.utils.translation import ugettext_lazy as _

## Mensaje a mostrar al usuario cuando el registro de datos haya sido ejecitado correctamente
CREATE_MESSAGE = _(u"Los datos fueron ingresados con éxito. ")

## Mensaje a mostrar cuando los datos hayan sido actualizados correctamente
UPDATE_MESSAGE = _(u"El registro fue actualizado con éxito. ")

## Mensaje a mostrar cuando los datos hayan sido eliminados correctamente
DELETE_MESSAGE = _(u"El registro seleccionado fue eliminado con éxito. ")

## Mensaje a mostrar cuando el usuario solicita una nueva contraseña
NUEVA_CLAVE_MESSAGE = _(u"¡La nueva contraseña fue enviada exitosamente a su dirección de correo electrónico")

## Límite de intentos fallidos para ingresar al sistema
INTENTOS_FALLIDOS = 3

INFO_BLOQUEO_MESSAGE = _(u"Usted será bloqueado al siguiente intento fallido.")

## Mensaje desautenticación en el sistema
LOGOUT_SECURITY_MESSAGE = _(u"Por su seguridad usted ha sido desautenticado del sistema, debe ingresar nuevamente.")

## Mensaje de error para peticiones AJAX
MSG_NOT_AJAX = _(u"No se puede procesar la petición. "
                 u"Verifique que posea las opciones javascript habilitadas e intente nuevamente.")

SEXO = [
	('', 'Seleccione...'),
	('M', 'Masculino'),
	('F', 'Femenino')
]

TIPO_NOVEDAD = [
	('AI', 'Alarma infundada'),
	('AP', 'Acto de Presencia'),
	('NA', 'Novedad Atendida')
]

DIVISION = [
	('CI', 'Combate de Incendios'),
	('BR', 'Búsqueda y Rescate'),
	('EP', 'Emergencia Pre-hospitalaria'),
	('AE', 'Actividades Especiales')
]

TIPO_UNIDAD = [
	('A', 'Ambulancia'),
	('R', 'Rescate'),
	('I', 'Incendio'),
	('M', 'Moto'),
	('O', 'Otros')
]

PERSONA_ESTATUS = [
	('L', 'Lesionado'),
	('F', 'Fallecido'),
	('P', 'Desaparecido'),
	('D', 'Desalojado'),
	('I', 'Ileso')
]

TIPO_VEHICULO = [
	('VH', 'Vehículo'),
	('PP', 'Por Puesto'),
	('TX', 'Taxi'),
	('CP', 'Carga Pesada'),
	('MT', 'Moto'),
	('OT', 'Otro')
]

TIPO_ESTRUCTURA = [
	('V', 'Vivienda'),
	('E', 'Edificio'),
	('L', 'Local Comercial'),
	('T', 'Terreno Baldío'),
	('O', 'Otro')
]

ROL_PERSONA_VEHICULO = [('C', 'Conductor'), ('A', 'Acompañante')]

TIPO_GRAFICO_REPORTE = [
	('', 'Seleccione...'),
	('A', 'Área'),
	('B', 'Barra'),
	('D', 'Dona'),
	#('L', 'Línea'),
	('T', 'Torta')
]