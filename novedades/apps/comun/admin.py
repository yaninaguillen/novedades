# coding=utf-8
from django.contrib import admin
from novedades.apps.comun.models import Estacion, Zona, Municipio, Parroquia, Unidad, Diagnostico
class ZonaAdmin(admin.ModelAdmin):
    list_display = ('nombre',)
    list_filter = ('nombre',)
    ordering = ('nombre',)
    search_fields = ('nombre',)

class MunicipioAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'zona')
    list_filter = ('nombre', 'zona')
    ordering = ('nombre', 'zona')
    search_fields = ('nombre', 'zona')

class ParroquiaAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'municipio')
    list_filter = ('nombre', 'municipio')
    ordering = ('nombre', 'municipio')
    search_fields = ('nombre', 'municipio')

class EstacionAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'jefe', 'direccion')
    list_filter = ('nombre', 'jefe')
    ordering = ('nombre', 'parroquia')
    search_fields = ('nombre', 'jefe', 'parroquia')

class UnidadAdmin(admin.ModelAdmin):
    list_display = ('estacion', 'tipo', 'codigo')
    list_filter = ('estacion', 'tipo')
    ordering = ('estacion', 'tipo', 'codigo')
    search_fields = ('estacion', 'tipo', 'codigo')

class DiagnosticoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'descripcion')
    list_filter = ('nombre',)
    ordering = ('nombre',)
    search_fields = ('nombre',)

admin.site.register(Estacion, EstacionAdmin)
admin.site.register(Zona, ZonaAdmin)
admin.site.register(Municipio, MunicipioAdmin)
admin.site.register(Parroquia, ParroquiaAdmin)
admin.site.register(Unidad, UnidadAdmin)
admin.site.register(Diagnostico, DiagnosticoAdmin)