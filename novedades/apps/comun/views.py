# coding=utf-8
from django.contrib.auth.decorators import login_required, user_passes_test
from django.utils.decorators import method_decorator
#from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.conf import settings
from django.core.management import call_command
from datetime import datetime
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, UpdateView, FormView
from django.contrib.messages.views import SuccessMessageMixin
from novedades.apps.comun.constantes import CREATE_MESSAGE, UPDATE_MESSAGE
from novedades.apps.comun.models import Comision, Estacion, Unidad, Diagnostico
from novedades.apps.comun.forms import ComisionForm, EstacionForm, UnidadForm, DiagnosticoForm

import os

@login_required()
def inicio(request):
    return render_to_response('base.html', {}, context_instance=RequestContext(request))

@login_required()
#@staff_member_required()
@user_passes_test(lambda u:u.is_staff, login_url='/')
def backup(request):
    respaldo = "exito"
    output_filename = os.path.join(settings.BASE_DIR, "novedades/backup/respaldo_%s.json" % datetime.now().strftime("%d-%m-%Y_%I-%M%p"))
    output = open(output_filename, "w")
    try:
        call_command('dumpdata', format='json', indent=4, stdout=output)
    except Exception, e:
        print e
        respaldo = "error"
    output.close()

    return render_to_response('base.html', {'respaldo': respaldo}, context_instance=RequestContext(request))

@login_required()
#@staff_member_required()
@user_passes_test(lambda u:u.is_staff, login_url='/')
def restore(request):
    loaddata = "exito"
    directorio = os.path.join(settings.BASE_DIR, "novedades/backup")
    archivos = []
    try:
        for root, dirs, files in os.walk(directorio):
            for file in files:
                if file.endswith(".json"):
                    archivos.append(os.path.join(root, file))

        archivos = sorted(archivos, reverse=True)
        call_command('loaddata', archivos[0])
    except Exception, e:
        print e
        loaddata = "error"

    return render_to_response('base.html', {'loaddata': loaddata}, context_instance=RequestContext(request))

@login_required()
@user_passes_test(lambda u:u.is_staff, login_url='/')
def consultar_estaciones(request):
    estaciones = Estacion.objects.all()

    return render_to_response("estacion_list.html", {'datos': estaciones}, context_instance=RequestContext(request))

@login_required()
@user_passes_test(lambda u:u.is_staff, login_url='/')
def consultar_unidades(request):
    unidades = Unidad.objects.all()

    return render_to_response("unidad_list.html", {'datos': unidades}, context_instance=RequestContext(request))

@login_required()
@user_passes_test(lambda u:u.is_staff, login_url='/')
def consultar_diagnosticos(request):
    diagnostico = Diagnostico.objects.all()

    return render_to_response("diagnostico_list.html", {'datos': diagnostico}, context_instance=RequestContext(request))

@login_required()
@user_passes_test(lambda u:u.is_staff, login_url='/')
def consultar_comisiones(request):
    comision = Comision.objects.all()

    return render_to_response("comision_list.html", {'datos': comision}, context_instance=RequestContext(request))


class ComisionCreate(SuccessMessageMixin, CreateView):
    model = Comision
    form_class = ComisionForm
    template_name = 'comision_registro.html'
    success_url = reverse_lazy('inicio')
    success_message = CREATE_MESSAGE

    @method_decorator(user_passes_test(lambda u: u.is_staff, login_url='/'))
    def dispatch(self, *args, **kwargs):
        return super(ComisionCreate, self).dispatch(*args, **kwargs)

class ComisionUpdate(SuccessMessageMixin, UpdateView):
    model = Comision
    form_class = ComisionForm
    template_name = 'comision_registro.html'
    success_url = reverse_lazy('inicio')
    success_message = UPDATE_MESSAGE

    @method_decorator(user_passes_test(lambda u: u.is_staff, login_url='/'))
    def dispatch(self, *args, **kwargs):
        return super(ComisionUpdate, self).dispatch(*args, **kwargs)

class EstacionCreate(SuccessMessageMixin, CreateView):
    model = Estacion
    form_class = EstacionForm
    template_name = 'estacion_registro.html'
    success_url = reverse_lazy('inicio')
    success_message = CREATE_MESSAGE

    @method_decorator(user_passes_test(lambda u: u.is_staff, login_url='/'))
    def dispatch(self, *args, **kwargs):
        return super(EstacionCreate, self).dispatch(*args, **kwargs)

class EstacionUpdate(SuccessMessageMixin, UpdateView):
    model = Estacion
    form_class = EstacionForm
    template_name = 'estacion_registro.html'
    success_url = reverse_lazy('inicio')
    success_message = UPDATE_MESSAGE

    @method_decorator(user_passes_test(lambda u: u.is_staff, login_url='/'))
    def dispatch(self, *args, **kwargs):
        return super(EstacionUpdate, self).dispatch(*args, **kwargs)

    def get_initial(self, **kwargs):

        return {
            'municipio': self.object.parroquia.municipio,
            'zona': self.object.parroquia.municipio.zona
        }

class UnidadCreate(SuccessMessageMixin, CreateView):
    model = Unidad
    form_class = UnidadForm
    template_name = 'unidad_registro.html'
    success_url = reverse_lazy('inicio')
    success_message = CREATE_MESSAGE

    @method_decorator(user_passes_test(lambda u: u.is_staff, login_url='/'))
    def dispatch(self, *args, **kwargs):
        return super(UnidadCreate, self).dispatch(*args, **kwargs)


class UnidadUpdate(SuccessMessageMixin, UpdateView):
    model = Unidad
    form_class = UnidadForm
    template_name = 'unidad_registro.html'
    success_url = reverse_lazy('inicio')
    success_message = UPDATE_MESSAGE

    @method_decorator(user_passes_test(lambda u: u.is_staff, login_url='/'))
    def dispatch(self, *args, **kwargs):
        return super(UnidadUpdate, self).dispatch(*args, **kwargs)

class DiagnosticoCreate(SuccessMessageMixin, CreateView):
    model = Diagnostico
    form_class = DiagnosticoForm
    template_name = 'diagnostico_registro.html'
    success_url = reverse_lazy('inicio')
    success_message = CREATE_MESSAGE

    @method_decorator(user_passes_test(lambda u: u.is_staff, login_url='/'))
    def dispatch(self, *args, **kwargs):
        return super(DiagnosticoCreate, self).dispatch(*args, **kwargs)

class DiagnosticoUpdate(SuccessMessageMixin, UpdateView):
    model = Diagnostico
    form_class = DiagnosticoForm
    template_name = 'diagnostico_registro.html'
    success_url = reverse_lazy('inicio')
    success_message = UPDATE_MESSAGE

    @method_decorator(user_passes_test(lambda u: u.is_staff, login_url='/'))
    def dispatch(self, *args, **kwargs):
        return super(DiagnosticoUpdate, self).dispatch(*args, **kwargs)