# coding=utf-8
from django.conf.urls import url, patterns
from django.contrib.auth.decorators import login_required
from novedades.apps.comun import views

urlpatterns = [
    url(r'^backup/$', 'novedades.apps.comun.views.backup', name='backup_data'),
    url(r'^restore/$', 'novedades.apps.comun.views.restore', name='restore_data'),
    url(r'^consulta_comision/$', 'novedades.apps.comun.views.consultar_comisiones', name='consulta_comision'),
    url(r'^registro_comision/$', login_required(views.ComisionCreate.as_view()), name='registro_comision'),
    url(r'^update_comision/(?P<pk>\d+)/$', login_required(views.ComisionUpdate.as_view()), name='modificar_comision'),
    url(r'^consulta_estacion/$', 'novedades.apps.comun.views.consultar_estaciones', name='consulta_estacion'),
    url(r'^registro_estacion/$', login_required(views.EstacionCreate.as_view()), name='registro_estacion'),
    url(r'^update_estacion/(?P<pk>\d+)/$', login_required(views.EstacionUpdate.as_view()), name='modificar_estacion'),
    url(r'^consulta_unidad/$', 'novedades.apps.comun.views.consultar_unidades', name='consulta_unidad'),
    url(r'^registro_unidad/$', login_required(views.UnidadCreate.as_view()), name = 'registro_unidad'),
    url(r'^update_unidad/(?P<pk>\d+)/$', login_required(views.UnidadUpdate.as_view()), name='modificar_unidad'),
    url(r'^consulta_diagnostico/$', 'novedades.apps.comun.views.consultar_diagnosticos', name='consulta_diagnostico'),
    url(r'^registro_diagnostico/$', login_required(views.DiagnosticoCreate.as_view()), name = 'registro_diagnostico'),
    url(r'^update_diagnostico/(?P<pk>\d+)/$', login_required(views.DiagnosticoUpdate.as_view()), name='modificar_diagnostico'),
    url(r'^ajax/actualizar_combo/?$', 'novedades.apps.comun.ajax.actualizar_combo', name='actualizar_combo'),
    url(r'^ajax/eliminar_registro/$', 'novedades.apps.comun.ajax.eliminar_registro', name="eliminar_registro"),
]