# coding=utf-8
from django.core.mail import send_mail
from django.template.loader import get_template
from django.template.context import Context
from django.conf import settings

def enviar_correo(email, template, subject, vars=None):
    if not vars:
        vars = {}

    try:
        ## Obtiene la plantilla de correo a implementar
        t = get_template(template)
        c = Context(vars)
        send_mail(subject, t.render(c), settings.EMAIL_FROM, [email], fail_silently=False)
        return True
    except smtplib.SMTPException, e:
        return False

def generar_password(size=8):
    from random import choice
    import string

    ## Conjunto de letras
    letras = string.letters

    ## Conjunto de dígitos
    numeros = string.digits

    ## Conjunto de signos
    signos = string.punctuation

    ## Generación de una nueva contraseña de acuerdo al tamaño indicado. Valor por defecto es de 8 carácteres.
    newpass = ''.join(choice([choice(letras), choice(numeros), choice(signos)]) for x in range(size))

    return newpass