# coding=utf-8

from django import forms
from django.utils.translation import ugettext_lazy as _
from novedades.apps.comun.models import Comision, Estacion, Unidad, Diagnostico, Zona, Municipio, Parroquia
from novedades.apps.comun.constantes import TIPO_UNIDAD

def cargarEstacion():
	lista = ('0', 'Seleccione...'),
	for estacion in Estacion.objects.all().order_by("nombre"):
		lista += (estacion.pk, estacion.nombre),
	return [lista]

def cargarZonas():
    lista = ('0', 'Seleccione...'),
    for zona in Zona.objects.all().order_by("nombre"):
        lista += (zona.pk, zona.nombre),
    return lista

class ComisionForm(forms.ModelForm):
	nombre = forms.CharField(label=_(u"Nombre"), max_length=30, widget=forms.TextInput(attrs={
        'placeholder': _(u"nombre de comisión"), 'class': 'form-control', 'onkeypress': 'return validar_letras(event);'
    }))

	class Meta:
		model = Comision
		fields = ['nombre',]

class EstacionForm(forms.ModelForm):
	zona = forms.ModelChoiceField(
        label=_(u"Zona"), queryset=Zona.objects.all(), empty_label=_(u"Seleccione..."),
        widget=forms.Select(attrs={
            'class': 'select2 span4',
            'title': _(u"Seleccione la zona de la estación"),
            'onchange': "actualizar_combo('/comun/ajax/actualizar_combo/', this.value, 'id_municipio', 'comun', 'Municipio', " \
            	"'zona', 'id', 'nombre', 'default')"
        })
    )
	
	municipio = forms.ModelChoiceField(
        label=_(u"Municipio"), queryset=Municipio.objects.all(), empty_label=_(u"Seleccione..."),
        widget=forms.Select(attrs={
            'class': 'select2 span4',
            'title': _(u"Seleccione el Municipio de la estación"),
            'onchange': "actualizar_combo('/comun/ajax/actualizar_combo/', this.value, 'id_parroquia', 'comun', 'Parroquia', " \
            	"'municipio', 'id', 'nombre', 'default')"
        })
    )

	parroquia = forms.ModelChoiceField(
        label=_(u"Parroquia"), queryset=Parroquia.objects.all(), empty_label=_(u"Seleccione..."),
        widget=forms.Select(attrs={
            'class': 'select2 span4',
            'title': _(u"Seleccione la parroquia a la que pertenece la estación")
        })
    )

	nombre = forms.CharField(label=_(u"Nombre"), max_length=60, widget=forms.TextInput(attrs={
		'placeholder': _(u"nombre de estación"), 'class': 'form-control span6', 'onkeypress': 'return validar_letras(event);'
	}))

	jefe = forms.CharField(label=_(u"Jefe"), max_length=50, widget=forms.TextInput(attrs={
		'placeholder': _(u"nombre del jefe"), 'class': 'form-control span6', 'onkeypress': 'return validar_letras(event);'
	}))

	telefono = forms.CharField(label=(u"Teléfono"), max_length=45, widget=forms.TextInput(attrs={
		'placeholder': _(u"teléfono de estación"), 'class': 'form-control span6', 'onkeypress': 'return validar_numeros(event);'
	}))

	direccion = forms.CharField(label=(u"Dirección"), max_length=255, widget=forms.Textarea(attrs={
		'class': 'form-control widget span6', 'rows': '3'
	}))

	principal = forms.ChoiceField(label=_(u"Principal"), choices=(('True', _(u"Sí")), ('False', _(u"No"))), widget=forms.RadioSelect(attrs={
		'class': 'radio', 'title': _(u"Indique si la estación es la principal")
	}))

	class Meta:
		model = Estacion
		fields = ['zona', 'municipio', 'parroquia', 'nombre', 'jefe', 'telefono', 'direccion', 'principal']


class UnidadForm(forms.ModelForm):
	tipo = forms.ChoiceField(label=_(u"Tipo"), choices=TIPO_UNIDAD, widget=forms.Select(attrs={
		'class': 'select2 span4', 'title': _(u'Seleccione el tipo de unidad')
	}))

	codigo = forms.CharField(label=_(u"Código"), max_length=10, widget=forms.TextInput(attrs={
		'placeholder': _(u"código de unidad"), 'class': 'form-control span6'
	}))

	estacion = forms.ModelChoiceField(
        label=_(u"Estación"), queryset=Estacion.objects.all(), empty_label=_(u"Seleccione..."),
        widget=forms.Select(attrs={
            'class': 'select2 span4',
            'title': _(u"Seleccione la estación a la que pertenece la unidad")
        })
    )

	class Meta:
		model = Unidad
		fields = ['estacion', 'tipo', 'codigo']

class DiagnosticoForm(forms.ModelForm):
	nombre = forms.CharField(label=_(u"Nombre"), max_length=60, widget=forms.TextInput(attrs={
		'placeholder': _(u"nombre del diagnóstico"), 'class': 'form-control', 'onkeypress': 'return validar_letras(event);'
	}))

	descripcion = forms.CharField(label=(u"Descripción"), max_length=255, widget=forms.Textarea(attrs={
		'class': 'form-control widget span6', 'rows': '3'
	}))

	class Meta:
		model = Diagnostico
		fields = ['nombre', 'descripcion']