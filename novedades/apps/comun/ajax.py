# coding=utf-8
from django.http import HttpResponse
from django.db import transaction
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy as _
from novedades.apps.comun.constantes import MSG_NOT_AJAX
from django.db.models.loading import get_model
import json

@login_required()
def actualizar_combo(request):
    try:
        if not request.is_ajax():
            return HttpResponse(json.dumps({'resultado': False, 'error': MSG_NOT_AJAX}))

        cod = request.GET.get('opcion', None)
        combo = request.GET.get('combo', None)
        app = request.GET.get('app', None)
        mod = request.GET.get('mod', None)
        campo = request.GET.get('campo', None)
        n_value = request.GET.get('n_value', None)
        n_text = request.GET.get('n_text', None)
        bd = request.GET.get('bd', None)

        if combo and app and mod and campo and n_value and n_text and bd:
            modelo = get_model(app, mod)
            filtro = {campo: cod}

            out = "<option value='0'>Seleccione...</option>"

            combo_disabled = "false"

            if cod != "" and cod != "0":
                for o in modelo.objects.using(bd).filter(**filtro).order_by(n_text):
                    out = "%s<option value='%s'>%s</option>" \
                          % (out, str(o.__getattribute__(n_value)),
                             o.__getattribute__(n_text).encode("utf-8"))
            else:
                combo_disabled = "true"

            return HttpResponse(json.dumps({'resultado': True, 'combo_disabled': combo_disabled, 'combo_html': out}))

        else:
            return HttpResponse(json.dumps({'resultado': False,
                                            'error': 'No se ha especificado el registro'}))

    except Exception, e:
        print e
        return HttpResponse(json.dumps({'resultado': False, 'error': e}))

#@transaction.commit_on_success_unless_managed()
def eliminar_registro(request):
    if not request.is_ajax():
        return HttpResponse(json.dumps({'resultado': False, 'msg': _(u'La solicitud no pudo ser procesada')}))

    app_label = request.GET.get('app_label', None)
    modelo = request.GET.get('modelo', None)
    id = request.GET.get('id', None)

    if app_label and modelo and id:
        try:
            tabla = get_model(app_label, modelo)
            registro = tabla.objects.get(pk=id)
            registro.delete()
        except Exception, e:
            return HttpResponse(json.dumps({'resultado': False, 'error': e}))

        return HttpResponse(json.dumps({'resultado': True}))
    return HttpResponse(json.dumps({'resultado': False, 'error': 'Registro no se puede eliminar'}))