# coding=utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from novedades.apps.comun.constantes import DIVISION, TIPO_UNIDAD

# Create your models here.

class Zona(models.Model):
	nombre = models.CharField(max_length=70)

	def __str__(self):
		return self.nombre

	def __unicode__(self):
		return self.nombre

class Municipio(models.Model):
	nombre = models.CharField(max_length=60)
	zona = models.ForeignKey(Zona)

	def __str__(self):
		return self.nombre


class Parroquia(models.Model):
	nombre = models.CharField(max_length=60)
	municipio = models.ForeignKey(Municipio)

	def __str__(self):
		return self.nombre

class Procedimiento(models.Model):
	nombre = models.CharField(max_length=60)
	division = models.CharField(max_length=2, choices=DIVISION)

	def __str__(self):
		return self.nombre

	def __unicode__(self):
		return self.nombre

class DetalleProcedimiento(models.Model):
	nombre = models.CharField(max_length=60)
	procedimiento = models.ForeignKey(Procedimiento)

	def __str__(self):
		return self.nombre

	def __unicode__(self):
		return self.nombre

class Comision(models.Model):
	nombre = models.CharField(max_length=60)

	def __str__(self):
		return self.nombre

	def __unicode__(self):
		return self.nombre

class Estacion(models.Model):
	nombre = models.CharField(max_length=70)
	jefe = models.CharField(max_length=60)
	telefono = models.CharField(max_length=15)
	direccion = models.CharField(max_length=255)
	principal = models.BooleanField(default=False)
	parroquia = models.ForeignKey(Parroquia)

	def __str__(self):
		return self.nombre

	def __unicode__(self):
		return self.nombre

	class Meta:
		verbose_name_plural = _(u"Estaciones")

class Unidad(models.Model):
	codigo = models.CharField(max_length=10)
	tipo = models.CharField(max_length=1, choices=TIPO_UNIDAD)
	estacion = models.ForeignKey(Estacion)

	def __str__(self):
		return self.codigo

class Diagnostico(models.Model):
	nombre = models.CharField(max_length=60)
	descripcion = models.CharField(max_length=255, null=True)

	def __str__(self):
		return self.nombre

	def __unicode__(self):
		return self.nombre