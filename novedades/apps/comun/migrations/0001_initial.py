# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Comision',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=60)),
            ],
        ),
        migrations.CreateModel(
            name='DetalleProcedimiento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=60)),
            ],
        ),
        migrations.CreateModel(
            name='Diagnostico',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=60)),
                ('descripcion', models.CharField(max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Estacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=60)),
                ('jefe', models.CharField(max_length=50)),
                ('telefono', models.CharField(max_length=45)),
                ('direccion', models.CharField(max_length=255)),
                ('principal', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name_plural': 'Estaciones',
            },
        ),
        migrations.CreateModel(
            name='Municipio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=60)),
            ],
        ),
        migrations.CreateModel(
            name='Parroquia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=60)),
                ('municipio', models.ForeignKey(to='comun.Municipio')),
            ],
        ),
        migrations.CreateModel(
            name='Procedimiento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=60)),
                ('division', models.CharField(max_length=2, choices=[(b'CI', b'Combate de Incendios'), (b'BR', b'B\xc3\xbasqueda y Rescate'), (b'EP', b'Emergencia Pre-hospitalaria'), (b'AE', b'Actividades Especiales')])),
            ],
        ),
        migrations.CreateModel(
            name='Unidad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('codigo', models.CharField(max_length=10)),
                ('tipo', models.CharField(max_length=1, choices=[(b'A', b'Ambulancia'), (b'R', b'Rescate'), (b'I', b'Incendio'), (b'O', b'Otros')])),
                ('estacion', models.ForeignKey(to='comun.Estacion')),
            ],
        ),
        migrations.CreateModel(
            name='Zona',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=70)),
            ],
        ),
        migrations.AddField(
            model_name='municipio',
            name='zona',
            field=models.ForeignKey(to='comun.Zona'),
        ),
        migrations.AddField(
            model_name='estacion',
            name='parroquia',
            field=models.ForeignKey(to='comun.Parroquia'),
        ),
        migrations.AddField(
            model_name='detalleprocedimiento',
            name='procedimiento',
            field=models.ForeignKey(to='comun.Procedimiento'),
        ),
    ]
