# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('comun', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='unidad',
            name='tipo',
            field=models.CharField(max_length=1, choices=[(b'A', b'Ambulancia'), (b'R', b'Rescate'), (b'I', b'Incendio'), (b'M', b'Moto'), (b'O', b'Otros')]),
        ),
    ]
