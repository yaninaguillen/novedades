from django.db import models

from novedades.apps.comun.models import Estacion
from django.contrib.auth.models import User

# Create your models here.

class UserProfile(models.Model):
    estacion = models.ForeignKey(Estacion)
    usuario = models.OneToOneField(User)