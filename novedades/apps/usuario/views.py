# coding=utf-8
from datetime import datetime
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin
from django.core import urlresolvers
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.conf import settings
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.views.generic import CreateView, UpdateView, FormView
from novedades.apps.usuario.models import UserProfile
from novedades.apps.usuario.forms import AutenticarForm, RegistroForm, PerfilForm, OlvidoPassForm, UpdateForm
from novedades.apps.comun.functions import enviar_correo, generar_password
from novedades.apps.comun.models import Estacion
from novedades.apps.comun.constantes import CREATE_MESSAGE, UPDATE_MESSAGE, INTENTOS_FALLIDOS, NUEVA_CLAVE_MESSAGE


def acceso(request):
    contador = 0
    if request.method == "POST":
        form = AutenticarForm(data=request.POST)

        if (request.POST['usuario']!="" or request.POST['clave']!="" or (request.POST['usuario']=="" and request.POST['passwd']=="")) and not 'contador_acceso' in request.session:
            request.session['contador_acceso'] = "0"
        else:
            contador = int(request.session['contador_acceso']) + 1
            request.session['contador_acceso'] = str(contador)

        if form.is_valid():
            request.session['contador_acceso'] = "0"
            usuario = authenticate(username=str(request.POST['usuario']), password=str(request.POST['clave']))
            login(request, usuario)
            usr = User.objects.get(username=request.POST['usuario'])
            usr.last_login = datetime.now()
            usr.save()
            return HttpResponseRedirect(urlresolvers.reverse("inicio"))
        else:
            if int(request.session['contador_acceso'])>(INTENTOS_FALLIDOS-1):
                del request.session['contador_acceso']
                if User.objects.filter(username=str(request.POST['usuario'])):
                    usr = User.objects.get(username=str(request.POST['usuario']))
                    usr.is_active=False
                    usr.save()
            return render_to_response('acceso.html', {'form': form}, context_instance=RequestContext(request))

    return render_to_response("acceso.html", {'form': AutenticarForm()}, context_instance=RequestContext(request))

@login_required()
def salir(request):
    user = request.user
    if user.is_authenticated():
        logout(request)
    return HttpResponseRedirect(urlresolvers.reverse("inicio"))

@login_required()
def consultar_usuarios(request):
    usuarios = User.objects.all()

    return render_to_response("usuario_list.html", {'datos': usuarios}, context_instance=RequestContext(request))


class RegistroCreate(SuccessMessageMixin, CreateView):
    model = UserProfile
    form_class = RegistroForm
    template_name = 'registro_usuario.html'
    success_url = reverse_lazy('inicio')
    success_message = CREATE_MESSAGE

    def form_valid(self, form):
        self.object = form.save(commit=False)
        try:
            
            estacion = Estacion.objects.get(nombre=form.cleaned_data['estacion'])
            usr = User.objects.create_user(
                username=form.cleaned_data['username'],
                email=form.cleaned_data['email'],
                first_name=form.cleaned_data['first_name'],
                last_name=form.cleaned_data['last_name']
            )
            usr.set_password(form.cleaned_data['password'])
            usr.save()
            form.instance.usuario = usr
            form.instance.estacion = estacion
            self.object.save()
        except Exception, e:
            print e

        return super(RegistroCreate, self).form_valid(form)


class OlvidoPassView(SuccessMessageMixin, FormView):
    form_class = OlvidoPassForm
    template_name = 'forgot_password.html'
    success_url = reverse_lazy('inicio')
    success_message = NUEVA_CLAVE_MESSAGE

    def form_valid(self, form):
        usr = User.objects.get(email=form.cleaned_data['email'])
        clave = generar_password()
        usr.set_password(clave)
        usr.save()
        enviado = enviar_correo(form.cleaned_data['email'], 'olvido_clave.mail', 'Nueva contraseña', {
            'usuario': usr.username, 'clave': clave, 'emailapp': settings.EMAIL_FROM
        })
        return super(OlvidoPassView, self).form_valid(form)


class UsuarioUpdate(SuccessMessageMixin, UpdateView):
    model = UserProfile
    form_class = UpdateForm
    template_name = 'registro_usuario.html'
    success_url = reverse_lazy('inicio')
    success_message = UPDATE_MESSAGE

    def get_initial(self, **kwargs):
        
        return {
            #'username': self.object.usuario.username,
            'first_name': self.object.usuario.first_name,
            'last_name': self.object.usuario.last_name,
            'email': self.object.usuario.email,
			'is_staff': self.object.usuario.is_staff,
            'is_active': self.object.usuario.is_active
        }

    # Falta incluir instrucciones para que guarde en la tabla de usuarios

class PerfilUpdate(SuccessMessageMixin, FormView):
    form_class = PerfilForm
    template_name = 'perfil_update.html'
    success_url = reverse_lazy('inicio')
    success_message = UPDATE_MESSAGE

    def get_initial(self):
        initial = super(PerfilUpdate, self).get_initial()
        initial['username'] = self.request.user.username
        initial['first_name'] = self.request.user.first_name
        initial['last_name'] = self.request.user.last_name
        initial['email'] = self.request.user.email
        return initial

    def form_valid(self, form):
        usr = User.objects.get(username=form.cleaned_data['username'])
        sv = False
        if usr.email != "" and usr.email != form.cleaned_data['email']:
            usr.email = form.cleaned_data['email']
            sv = True
        if form.cleaned_data['password'] != "":
            usr.set_password(form.cleaned_data['password'])
            sv = True
        if sv:
            usr.save()
        return super(PerfilUpdate, self).form_valid(form)