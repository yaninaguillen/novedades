# coding=utf-8

from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from novedades.apps.comun.models import Estacion
from novedades.apps.usuario.models import UserProfile


class AutenticarForm(forms.Form):
    usuario = forms.CharField(label=_(u"Usuario"), max_length=30, widget=forms.TextInput(attrs={
        'placeholder': _(u"nombre de usuario"), 'class': 'login username-field', 'onkeypress': 'return validar_letras(event);'
    }))

    clave = forms.CharField(label=_(u"Contraseña"), max_length=30, widget=forms.PasswordInput(attrs={
        'placeholder': _(u"contraseña de acceso"), 'class': 'login password-field'
    }))

    def clean_usuario(self):
        usuario = self.cleaned_data['usuario']
        if not User.objects.filter(username=usuario):
            raise forms.ValidationError(_(u"El usuario no existe"))
        return usuario

    def clean_clave(self):
        clave = self.cleaned_data['clave']
        if User.objects.filter(username=self.data['usuario']):
            usr = User.objects.get(username=self.data['usuario'])
            if not usr.check_password(clave):
                raise forms.ValidationError(_(u"La contraseña no coincide"))
        return clave


class OlvidoPassForm(forms.Form):
    email = forms.CharField(label=_(u"Correo"), max_length=75, widget=forms.TextInput(attrs={
        'placeholder': 'usuario@dominio.com', 'class': 'login'
    }))

    def clean_email(self):
        correo = self.cleaned_data['email']
        print "entro"
        if not User.objects.filter(email=correo):
            raise forms.ValidationError(_(u"El correo no esta registrado"))
        return correo



class RegistroForm(forms.ModelForm):

    estacion = forms.ModelChoiceField(
        label=_(u"Estación"), queryset=Estacion.objects.all(), empty_label=_(u"Seleccione..."),
        widget=forms.Select(attrs={
            'class': 'select2 span4',
            'title': _(u"Seleccione la estación a la que pertenece el usuario")
        })
    )

    username = forms.CharField(label=_(u"Usuario"), max_length=30, widget=forms.TextInput(attrs={
        'placeholder': _(u"nombre de usuario"), 'class': 'login', 'onkeypress': 'return validar_letras(event);'
    }))

    password = forms.CharField(label=_(u"Contraseña"), max_length=30, widget=forms.PasswordInput(attrs={
        'placeholder': _(u"contraseña de acceso"), 'class': 'login', 'onkeyup': 'passwordStrength(this.value)'
    }))

    first_name = forms.CharField(label=_(u"Nombres"), max_length=30, widget=forms.TextInput(attrs={
        'placeholder': _(u"nombre completo"), 'class': 'login', 'onkeypress': 'return validar_letras(event);'
    }), required=True)

    last_name = forms.CharField(label=_(u"Apellidos"), max_length=30, widget=forms.TextInput(attrs={
        'placeholder': _(u"apellido completo"), 'class': 'login', 'onkeypress': 'return validar_letras(event);'
    }), required=True)

    email = forms.EmailField(label=_(u"correo electronico"), max_length=75, widget=forms.TextInput(attrs={
        'placeholder': 'usuario@dominio', 'class': 'login'
    }), required=True)

    is_staff = forms.ChoiceField(label=_(u"Administrador"), choices=(('True', _(u"Sí")), ('False', _(u"No"))),widget=forms.RadioSelect(attrs={
        'class': 'radio', 'title': _(u"Indique si el usuario es administrador")
    }))

    is_active = forms.ChoiceField(label=_(u"Activo"), choices=(('True', _(u"Sí")), ('False', _(u"No"))), widget= forms.RadioSelect(attrs={
        'class': 'radio', 'title': _(u"Indique si el usuario está activo")
    }))

    def clean_username(self):
        if User.objects.filter(username = self.cleaned_data['username']):
            raise forms.ValidationError(_(u"El nombre de usuario ya existe"))
        return self.cleaned_data['username']

    class Meta:
        model = UserProfile
        exclude = ['is_superuser', 'last_login', 'groups', 'user_permissions', 'date_joined', 'username']
        fields = ['estacion', 'first_name', 'last_name', 'email', 'username', 'password']


class UpdateForm(forms.ModelForm):
    estacion = forms.ModelChoiceField(
        label=_(u"Estación"), queryset=Estacion.objects.all(), empty_label=_(u"Seleccione..."),
        widget=forms.Select(attrs={
            'class': 'select2 span4',
            'title': _(u"Seleccione la estación a la que pertenece el usuario")
    }))

    password = forms.CharField(label=_(u"Contraseña"), max_length=30, widget=forms.PasswordInput(attrs={
        'placeholder': _(u"contraseña de acceso"), 'class': 'login', 'onkeyup': 'passwordStrength(this.value)'
    }))

    first_name = forms.CharField(label=_(u"Nombres"), max_length=30, widget=forms.TextInput(attrs={
        'placeholder': _(u"nombre completo"), 'class': 'login', 'onkeypress': 'return validar_letras(event);'
    }), required=True)

    last_name = forms.CharField(label=_(u"Apellidos"), max_length=30, widget=forms.TextInput(attrs={
        'placeholder': _(u"apellido completo"), 'class': 'login', 'onkeypress': 'return validar_letras(event);'
    }), required=True)

    email = forms.EmailField(label=_(u"correo electronico"), max_length=75, widget=forms.TextInput(attrs={
        'placeholder': 'usuario@dominio', 'class': 'login'
    }), required=True)

    is_staff = forms.ChoiceField(label=_(u"Administrador"), choices=(('True', _(u"Sí")), ('False', _(u"No"))),widget=forms.RadioSelect(attrs={
        'class': 'radio', 'title': _(u"Indique si el usuario es administrador")
    }))

    is_active = forms.ChoiceField(label=_(u"Activo"), choices=(('True', _(u"Sí")), ('False', _(u"No"))), widget= forms.RadioSelect(attrs={
        'class': 'radio', 'title': _(u"Indique si el usuario está activo")
    }))

    class Meta:
        model = UserProfile
        exclude = ['is_superuser', 'last_login', 'groups', 'user_permissions', 'date_joined', 'username']
        fields = ['estacion', 'first_name', 'last_name', 'email', 'username', 'password']



class PerfilForm(forms.Form):

    username = forms.CharField(widget=forms.HiddenInput(attrs={'readonly':'readonly', 'display': 'none'}), required=False)

    first_name = forms.CharField(label=_(u"Nombres"), max_length=30, widget=forms.TextInput(attrs={
        'readonly': 'readonly', 'class': 'login span4'
    }))

    last_name = forms.CharField(label=_(u"Apellidos"), max_length=30, widget=forms.TextInput(attrs={
        'readonly': 'readonly', 'class': 'login'
    }))

    email = forms.EmailField(label=_(u"correo electronico"), max_length=75, widget=forms.TextInput(attrs={
        'placeholder': 'usuario@dominio', 'class': 'login'
    }), required=True)

    old_password = forms.CharField(label=_(u"Contraseña Anterior"), max_length=30, widget=forms.PasswordInput(attrs={
        'placeholder': _(u"contraseña de acceso a modificar"), 'class': 'login'
    }))

    password = forms.CharField(label=_(u"Nueva Contraseña"), max_length=30, widget=forms.PasswordInput(attrs={
        'placeholder': _(u"nueva contraseña de acceso"), 'class': 'login', 'onkeyup': 'passwordStrength(this.value)'
    }))

    confirm_password = forms.CharField(label=_(u"Confirmar Contraseña"), max_length=30, widget=forms.PasswordInput(attrs={
        'placeholder': _(u"confirmar contraseña de acceso"), 'class': 'login'
    }))


    def clean_old_password(self):
        old_password = self.cleaned_data['old_password']
        usr = User.objects.get(username=self.data['username'])
        if not usr.check_password(old_password):
            raise forms.ValidationError(_(u"La contraseña no coincide"))
        return old_password

    def clean_password(self):
        password_strength = int(self.data['passwordMeterId'])
        if password_strength < 3:
            raise forms.ValidationError(_(u"La contraseña es muy débil"))
        return self.cleaned_data['password']

    def clean_confirm_password(self):
        password = self.data['password']
        confirm_password = self.cleaned_data['confirm_password']
        if password != confirm_password:
            raise forms.ValidationError(_(u"La nueva contraseña no puede ser confirmada"))
        return confirm_password
