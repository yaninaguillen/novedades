from django.conf.urls import url, patterns
from django.contrib.auth.decorators import login_required
from novedades.apps.usuario import views

urlpatterns = [
    url(r'^registro/$', login_required(views.RegistroCreate.as_view()), name='registro_usuario'),
    url(r'^update_usuario/(?P<pk>\d+)/$', login_required(views.UsuarioUpdate.as_view()), name='modificar_usuario'),
    url(r'^perfil/(?P<pk>\d+)/$', login_required(views.PerfilUpdate.as_view()), name='perfil_usuario'),
    url(r'^consulta_usuario/$', 'novedades.apps.usuario.views.consultar_usuarios', name='consulta_usuario'),
]