# coding=utf-8
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.core import urlresolvers
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect

from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.views.generic import CreateView, UpdateView, FormView
from novedades.apps.novedad.forms import NovedadForm, ReporteForm, FiltroListForm #, NovedadEmergenciaPrehospitalariaForm, NovedadBusquedaRescateForm
from novedades.apps.novedad.models import Novedad, NovedadUnidad, NovedadComision, Persona, NovedadPersona, PersonaDiagnostico, \
Vehiculo, NovedadVehiculo, Estructura, PersonaEstructura
from novedades.apps.comun.models import Unidad, Diagnostico
from novedades.apps.comun.constantes import CREATE_MESSAGE, UPDATE_MESSAGE

from datetime import datetime

@login_required()
def reporte_general(request):
    form = ReporteForm()
    datos, division = [], []
    if request.POST:
        form = ReporteForm(request.POST)
        if form.is_valid():
            anho, mes = form.cleaned_data['anho'], form.cleaned_data['mes']
            filtro = {'fecha__year': anho}
            division = ['CI','BR','EP','AE']
            
            for d in division:
                filtro['procedimiento__division'] = d
                novedad_x_mes = []
                if mes:
                    filtro['fecha__month'] = mes
                    novedad = Novedad.objects.filter(**filtro)
                else:
                    for m in range(1,13):
                        filtro['fecha__month'] = m
                        novedad_x_mes.append(Novedad.objects.filter(**filtro).count())

                datos.append({d:novedad_x_mes})

                #datos = Novedad.objects.filter(procedimiento__division=d)
            #datos = Novedad.objects.filter(**filtro)
    return render_to_response("reporte_general.html", {
        'form':form, 'datos':datos, 'division':division
    }, context_instance=RequestContext(request))

class NovedadCreate(SuccessMessageMixin, FormView):
    form_class = NovedadForm
    template_name = 'base_novedad.html'
    success_url = reverse_lazy('inicio')
    success_message = CREATE_MESSAGE

    def form_valid(self, form):
        ## Logica de registro de novedades
        # Campos
        #fecha = form.cleaned_data['fecha']
        fecha = datetime.now()
        hora_salida = form.cleaned_data['hora_salida']
        hora_llegada = form.cleaned_data['hora_llegada']
        hora_reporte = form.cleaned_data['hora_reporte']
        estacion = form.cleaned_data['estacion']
        direccion = form.cleaned_data['direccion']
        tipo_novedad = form.cleaned_data['tipo_novedad']
        tipo_procedimiento = form.cleaned_data['tipo_procedimiento']
        detalle_procedimiento = form.cleaned_data['detalle_procedimiento']

        ## Registro de los datos básicos de la Novedad ==============================================================

        novedad = Novedad.objects.create(
            fecha=fecha, hora_salida=hora_salida, hora_llegada=hora_llegada, hora_reporte=hora_reporte,
            estacion=estacion, direccion=direccion, tipo=tipo_novedad, descripcion=detalle_procedimiento, 
            procedimiento=tipo_procedimiento
        )


        ## Registro de Unidades ======================================================================================
        codigo_unidad = form.cleaned_data['codigo_unidad']
        responsable_unidad = form.cleaned_data['responsable_unidad']

        novedad_unidad = NovedadUnidad.objects.create(unidad=codigo_unidad, responsable=responsable_unidad, novedad=novedad)

        ## Si hay datos 

        ## Registro de comisiones =====================================================================================
        if form.cleaned_data['nombre_comision'] and (form.cleaned_data['nombre_comision']!="0" or form.cleaned_data['nombre_comision']!=""):
            nombre_comision = form.cleaned_data['nombre_comision']
            responsable_comision = form.cleaned_data['responsable_comision']
            novedad_comision = NovedadComision.objects.create(
                comision=nombre_comision, responsable=responsable_comision, novedad=novedad
            )

        ## Registro de personas =======================================================================================
        if form.cleaned_data['persona_cedula'] and form.cleaned_data['persona_cedula']!="":
            identificado = form.cleaned_data['identificado']
            persona_cedula = form.cleaned_data['persona_cedula']
            persona_nombre = form.cleaned_data['persona_nombre']
            persona_sexo = form.cleaned_data['persona_sexo']
            persona_edad = form.cleaned_data['persona_edad']
            persona_estatus = form.cleaned_data['persona_estatus']
            persona_diagnostico = form.cleaned_data['persona_diagnostico']
            persona_observaciones = form.cleaned_data['persona_observaciones']

            ## Obtiene los datos de la persona si ya se encuentra registrada en el sistema
            if Persona.objects.filter(cedula=persona_cedula):
                persona = Persona.objects.get(cedula=persona_cedula)
            else:
                ## Crea la persona si no esta registrada
                persona = Persona.objects.create(
                    nombre=persona_nombre, cedula=persona_cedula, edad=persona_edad, sexo=persona_sexo,
                    identificado=identificado
                )

            novedad_persona = NovedadPersona.objects.create(
                observacion=persona_observaciones, persona=persona, novedad=novedad
            )
            diagnostico=Diagnostico.objects.get(pk=persona_diagnostico)
            persona_diagnostico = PersonaDiagnostico.objects.create(
                estatus=persona_estatus, diagnostico=diagnostico, persona=persona
            )

        ## Registro de vehiculos =======================================================================================
        if form.cleaned_data['tipo_vehiculo'] and (form.cleaned_data['tipo_vehiculo']!="0" or form.cleaned_data['tipo_vehiculo']!=""):
            placa_vehiculo = form.cleaned_data['placa_vehiculo']
            tipo_vehiculo = form.cleaned_data['tipo_vehiculo']
            marca_vehiculo = form.cleaned_data['marca_vehiculo']
            modelo_vehiculo = form.cleaned_data['modelo_vehiculo']
            anho_vehiculo = form.cleaned_data['anho_vehiculo']
            color_vehiculo = form.cleaned_data['color_vehiculo']
            observaciones_vehiculo = form.cleaned_data['observaciones_vehiculo']
            vehiculo = Vehiculo.objects.create(
                tipo=tipo_vehiculo, marca=marca_vehiculo, modelo=modelo_vehiculo,
                anno=anho_vehiculo, placa=placa_vehiculo, color=color_vehiculo
            )
            novedad_vehiculo = NovedadVehiculo.objects.create(
                novedad=novedad, vehiculo=vehiculo, observacion=observaciones_vehiculo
            )

        ## Registro de estructuras =====================================================================================
        if form.cleaned_data['tipo_estructura'] and (form.cleaned_data['tipo_estructura']!="0" or form.cleaned_data['tipo_estructura']!=""):
            tipo_estructura = form.cleaned_data['tipo_estructura']
            observaciones_estructura = form.cleaned_data['observaciones_estructura']
            estructura = Estructura.objects.create(
                novedad=novedad, tipo=tipo_estructura, observacion=observaciones_estructura
            )
            if persona:
                persona_estructura = PersonaEstructura.objects.create(
                    persona=persona, estructura=estructura
                )


        return super(NovedadCreate, self).form_valid(form)

class NovedadUpdate(SuccessMessageMixin, UpdateView):
    model = Novedad
    form_class = NovedadForm
    template_name = 'novedad_registro.html'
    success_url = reverse_lazy('inicio')
    success_message = UPDATE_MESSAGE


@login_required()
def consultar_novedades(request):
    novedad = Novedad.objects.filter(fecha__year=datetime.now().year, fecha__month=datetime.now().month)
    form = FiltroListForm()
    if request.POST:
        filtro = {
            "fecha__year":datetime.now().year,
            "fecha__month":datetime.now().month
        }
        form = FiltroListForm(request.POST)
        if request.POST['anho']:
            filtro["fecha__year"] = request.POST['anho']
        if request.POST['mes']:
            filtro["fecha__month"] = request.POST['mes']
        
        if request.POST['tipo_novedad']:
            filtro['tipo'] = request.POST['tipo_novedad']
        novedad = Novedad.objects.filter(**filtro)

    return render_to_response("novedad_list.html", {'datos': novedad, 'form':form}, context_instance=RequestContext(request))

"""class NovedadEmergenciaPrehospitalariaCreate(SuccessMessageMixin, FormView):
    form_class = NovedadEmergenciaPrehospitalariaForm
    template_name = 'novedad_emergencia_prehosp.html'
    success_url = reverse_lazy('inicio')
    success_message = UPDATE_MESSAGE

class NovedadBusquedaRescateCreate(SuccessMessageMixin, FormView):
    form_class = NovedadBusquedaRescateForm
    template_name = 'novedad_emergencia_prehosp.html'
    success_url = reverse_lazy('inicio')
    success_message = UPDATE_MESSAGE"""