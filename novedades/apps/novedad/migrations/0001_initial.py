# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('comun', '0002_auto_20150929_2001'),
    ]

    operations = [
        migrations.CreateModel(
            name='Estructura',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.CharField(max_length=2, choices=[(b'V', b'Vivienda'), (b'E', b'Edificio'), (b'L', b'Local Comercial'), (b'T', b'Terreno Bald\xc3\xado'), (b'O', b'Otro')])),
                ('observacion', models.CharField(max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Novedad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha', models.DateField()),
                ('hora_salida', models.TimeField()),
                ('hora_llegada', models.TimeField()),
                ('hora_reporte', models.TimeField()),
                ('direccion', models.CharField(max_length=255)),
                ('descripcion', models.CharField(max_length=255, null=True)),
                ('tipo', models.CharField(max_length=2, choices=[(b'AI', b'Alarma infundada'), (b'AP', b'Acto de Presencia'), (b'NA', b'Novedad Atendida')])),
                ('estacion', models.ForeignKey(to='comun.Estacion')),
                ('procedimiento', models.ForeignKey(to='comun.Procedimiento')),
            ],
        ),
        migrations.CreateModel(
            name='NovedadComision',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('responsable', models.CharField(max_length=60, null=True)),
                ('comision', models.ForeignKey(to='comun.Comision')),
                ('novedad', models.ForeignKey(to='novedad.Novedad')),
            ],
        ),
        migrations.CreateModel(
            name='NovedadPersona',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('observacion', models.CharField(max_length=255, null=True)),
                ('novedad', models.ForeignKey(to='novedad.Novedad')),
            ],
        ),
        migrations.CreateModel(
            name='NovedadUnidad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('responsable', models.CharField(max_length=60, null=True)),
                ('novedad', models.ForeignKey(to='novedad.Novedad')),
                ('unidad', models.ForeignKey(to='comun.Unidad')),
            ],
        ),
        migrations.CreateModel(
            name='NovedadVehiculo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('observacion', models.CharField(max_length=255, null=True)),
                ('novedad', models.ForeignKey(to='novedad.Novedad')),
            ],
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=60, null=True)),
                ('cedula', models.CharField(max_length=12, unique=True, null=True)),
                ('edad', models.SmallIntegerField(null=True)),
                ('sexo', models.CharField(max_length=1, null=True, choices=[(b'M', b'Masculino'), (b'F', b'Femenino')])),
                ('identificado', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='PersonaDiagnostico',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('estatus', models.CharField(max_length=1, choices=[(b'L', b'Lesionado'), (b'F', b'Fallecido'), (b'P', b'Desaparecido'), (b'D', b'Desalojado'), (b'I', b'Ileso')])),
                ('diagnostico', models.ForeignKey(to='comun.Diagnostico')),
                ('persona', models.ForeignKey(to='novedad.Persona')),
            ],
        ),
        migrations.CreateModel(
            name='PersonaEstructura',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('propietario', models.BooleanField()),
                ('estructura', models.ForeignKey(to='novedad.Estructura')),
                ('persona', models.ForeignKey(to='novedad.Persona')),
            ],
        ),
        migrations.CreateModel(
            name='PersonaVehiculo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo_persona', models.CharField(max_length=1, choices=[(b'C', b'Conductor'), (b'A', b'Acompa\xc3\xb1ante')])),
                ('persona', models.ForeignKey(to='novedad.Persona')),
            ],
        ),
        migrations.CreateModel(
            name='Vehiculo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.CharField(max_length=2)),
                ('marca', models.CharField(max_length=50)),
                ('modelo', models.CharField(max_length=50)),
                ('anno', models.CharField(max_length=5)),
                ('placa', models.CharField(max_length=10)),
                ('color', models.CharField(max_length=15)),
            ],
        ),
        migrations.AddField(
            model_name='personavehiculo',
            name='vehiculo',
            field=models.ForeignKey(to='novedad.Vehiculo'),
        ),
        migrations.AddField(
            model_name='novedadvehiculo',
            name='vehiculo',
            field=models.ForeignKey(to='novedad.Vehiculo'),
        ),
        migrations.AddField(
            model_name='novedadpersona',
            name='persona',
            field=models.ForeignKey(to='novedad.Persona'),
        ),
        migrations.AddField(
            model_name='estructura',
            name='novedad',
            field=models.ForeignKey(to='novedad.Novedad'),
        ),
    ]
