# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('novedad', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personaestructura',
            name='propietario',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='personavehiculo',
            name='tipo_persona',
            field=models.CharField(max_length=1, null=True, choices=[(b'C', b'Conductor'), (b'A', b'Acompa\xc3\xb1ante')]),
        ),
    ]
