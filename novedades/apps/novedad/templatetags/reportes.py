# coding=utf-8
from django import template
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
register = template.Library()

@register.simple_tag
def reporte_general(datos):
	tbody, input_hide = '', ''

	if datos:
		total_mes = [0 for t in range(13)]
		for dat in datos:
			key, total = '', 0
			tbody += "<tr>"
			
			if dat.has_key('CI'):
				key = 'ci'
				tbody += "<td>%s</td>" % _(u"Combate de Incendios")
			elif dat.has_key('BR'):
				key = 'br'
				tbody += "<td>%s</td>" % _(u"Búsqueda y Rescate")
			elif dat.has_key('EP'):
				key = 'ep'
				tbody += "<td>%s</td>" % _(u"Emergencias Pre-Hospitalarias")
			elif dat.has_key('AE'):
				key = 'ae'
				tbody += "<td>%s</td>" % _(u"Actividades Especiales")

			i=0
			for val in dat.values()[0]:
				tbody += "<td id='%s-%s'>%s</td>" % (key, str(i), str(val))
				total += val
				total_mes[i] = total_mes[i] + val
				i += 1
			total_mes[-1] = total_mes[-1] + total
			input_hide += "<input type='hidden' id='%s-total-graficar' value='%s'/>" % (key, str(total))
			tbody += "<td id='%s-total'>%s</td></tr>" % (key, str(total))
		tbody += "<tr><td><h3>TOTAL</h3></td>"
		x=0
		for tm in total_mes:
			tbody += "<td id='total-%s'><b>%s</b></td>" % (str(x), str(tm))
			x += 1
		tbody += "</tr>"
	
	tbody = input_hide+tbody
	
	return mark_safe(tbody)