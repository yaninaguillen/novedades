# coding=utf-8

from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from novedades.apps.comun.models import Zona, Comision, Municipio, Parroquia, Procedimiento, Estacion, Diagnostico, Unidad
from novedades.apps.comun.constantes import TIPO_UNIDAD, SEXO, TIPO_NOVEDAD, DIVISION, PERSONA_ESTATUS, TIPO_VEHICULO, TIPO_ESTRUCTURA, \
TIPO_GRAFICO_REPORTE
from novedades.apps.novedad.models import Novedad

def cargarZonas():
    lista = ('', 'Seleccione...'),
    for zona in Zona.objects.all().order_by("nombre"):
        lista += (zona.pk, zona.nombre),
    return lista

def cargarComision():
	lista = ('', 'Seleccione...'),
	for comision in Comision.objects.all().order_by("nombre"):
		lista += (comision.pk, comision.nombre),
	return lista

def cargarDiagnostico():
	lista = ('', 'Seleccione...'),
	for diag in Diagnostico.objects.all().order_by("nombre"):
		lista += (diag.pk, diag.nombre),
	return lista

def cargarAnhosNovedades():
	lista = ('', 'Seleccione...'),
	for nov in Novedad.objects.dates("fecha", "year"):
		lista += (nov.year, nov.year),
	return lista

def cargarMesesNovedades():
	lista = ('', 'Seleccione...'),
	meses = {
		1:'Enero', 2:'Febrero', 3:'Marzo', 4:'Abril', 5:'Mayo', 6:'Junio', 7:'Julio', 8:'Agosto', 9:'Septiembre', 10:'Octubre',
		11:'Noviembre', 12:'Diciembre'
	}
	mes_existe = []
	for nov in Novedad.objects.dates("fecha", "month"):
		if not nov.month in mes_existe:
			mes_existe.append(nov.month)
			lista += (nov.month, meses[nov.month]),
	return lista


class UnidadForm(forms.Form):
	# Formulario de Unidades

	tipo_unidad = forms.ChoiceField(label=_(u"Tipo de Unidad"), choices=[('','Seleccione...')]+TIPO_UNIDAD, widget=forms.Select(attrs={
		'class': 'select2 form-control', 'title': _(u'Seleccione el tipo de unidad'),
		'onchange': "actualizar_combo('/comun/ajax/actualizar_combo/', this.value, 'id_codigo_unidad', 'comun', 'Unidad', " \
					"'tipo', 'id', 'codigo', 'default')"
	}))

	codigo_unidad = forms.ModelChoiceField(
        label=_(u"Número"), queryset=Unidad.objects.all(), empty_label=_(u"Seleccione..."),
        widget=forms.Select(attrs={
            'class': 'select2 span3',
            'title': _(u"Seleccione el número de la unidad")
        })
    )

	responsable_unidad = forms.CharField(label=_(u"Responsable"), widget=forms.TextInput(attrs={
		'class': 'span3', 'onkeypress': 'return validar_letras(event);'
	}))


class ComisionForm(forms.Form):
	# Formulario de comisiones
	nombre_comision = forms.ModelChoiceField(
        label=_(u"Nombre"), queryset=Comision.objects.all(), empty_label=_(u"Seleccione..."),
        widget=forms.Select(attrs={
            'class': 'select2 span3',
            'title': _(u"Seleccione el nombre de la comisión")
        }), required=False
    )

	responsable_comision = forms.CharField(label=_(u"Responsable"), widget=forms.TextInput(attrs={
		'class': 'span3', 'onkeypress': 'return validar_letras(event);'
	}), required=False)

	def __init__(self, *args, **kwargs):
		super(ComisionForm, self).__init__(*args, **kwargs)
		## Condiciones que evaluan si se ha especificado información en los campos,
		#  en caso afirmativo establece los mismos como requeridos
		if self.data:
			if self.data['nombre_comision']:
				self.fields['responsable_comision'].required = True
			elif self.data['responsable_comision'] and self.data['responsable_comision']!="":
				self.fields['nombre_comision'].required = True
		

class PersonaForm(forms.Form):
	identificado = forms.ChoiceField(label=_(u"Identificado"), choices=((True, _(u"Sí")), (False, _(u"No"))), 
		                             widget=forms.RadioSelect(attrs={
		                             	'class': 'radio', 'title': _(u"Indique si la persona fue identificada"),
		                             	'style': 'margin-left:10px; margin-right: 10px;'
		                             }), required=False)
	persona_cedula = forms.CharField(label=_(u"Cédula"), widget=forms.TextInput(attrs={
		'class': 'span2', 'onkeypress': 'return validar_solo_numeros(event);'
	}), required=False)

	persona_nombre = forms.CharField(label=_(u"Nombre"), widget=forms.TextInput(attrs={
		'class': 'span3', 'onkeypress': 'return validar_letras(event);'
	}), required=False)

	persona_sexo = forms.ChoiceField(label=_(u"Sexo"), choices=SEXO, widget=forms.Select(attrs={
		'class': 'select2 form-control', 'title': _(u'Seleccione el sexo de la persona')
	}), required=False)

	persona_edad = forms.CharField(label=_(u"Edad"), widget=forms.TextInput(attrs={
		'class': 'span2', 'onkeypress': 'return validar_solo_numeros(event);'
	}), required=False)

	persona_estatus = forms.ChoiceField(label=_(u"Estatus"), choices=[('','Seleccione...')]+PERSONA_ESTATUS, widget=forms.Select(attrs={
		'class': 'select2 form-control span3', 'title': _(u'Seleccione el estado en que se encuentra la persona')
	}), required=False)

	persona_diagnostico = forms.ChoiceField(label=_(u"Diagnóstico"), choices=cargarDiagnostico, widget=forms.Select(attrs={
		'class': 'select2 form-control span3', 'title': _(u'Seleccione el diagnóstico de la persona')
	}), required=False)

	persona_observaciones = forms.CharField(label=_(u"Nombre"), widget=forms.Textarea(attrs={
		'class': 'form-control widget span6', 'rows': '3'
	}), required=False)

	def __init__(self, *args, **kwargs):
		super(PersonaForm, self).__init__(*args, **kwargs)

		## Condiciones que evaluan si se ha especificado información en los campos,
		#  en caso afirmativo establece los mismos como requeridos
		if self.data:
			if self.data['persona_cedula'] and self.data['persona_cedula']!="":
				self.fields['identificado'].required = True
				self.fields['persona_nombre'].required = True
				self.fields['persona_sexo'].required = True
				self.fields['persona_edad'].required = True
				self.fields['persona_estatus'].required = True
				self.fields['persona_diagnostico'].required = True
				self.fields['persona_observaciones'].required = True


class VehiculoForm(forms.Form):
	placa_vehiculo = forms.CharField(label=_(u"Placa"), widget=forms.TextInput(attrs={
		'class': 'span2', 'maxlength': '10'
	}), required=False)

	tipo_vehiculo = forms.ChoiceField(label=_(u"Tipo"), choices=[('','Seleccione...')]+TIPO_VEHICULO, widget=forms.Select(attrs={
		'class': 'select2 form-control span3', 'title': _(u'Seleccione el el tipo de vehículo')
	}), required=False)

	marca_vehiculo = forms.CharField(label=_(u"Marca"), widget=forms.TextInput(attrs={
		'class': 'span2'
	}), required=False)

	modelo_vehiculo = forms.CharField(label=_(u"Modelo"), widget=forms.TextInput(attrs={
		'class': 'span2'
	}), required=False)

	anho_vehiculo = forms.CharField(label=_(u"Año"), widget=forms.TextInput(attrs={
		'class': 'span2', 'onkeypress': 'return validar_solo_numeros(event);', 'maxlength': '4'
	}), required=False)

	color_vehiculo = forms.CharField(label=_(u"Color"), widget=forms.TextInput(attrs={
		'class': 'span2', 'onkeypress': 'return validar_letras(event);'
	}), required=False)

	observaciones_vehiculo = forms.CharField(label=_(u"Observaciones"), widget=forms.Textarea(attrs={
		'class': 'form-control widget span6', 'rows': '3'
	}), required=False)

	def __init__(self, *args, **kwargs):
		super(VehiculoForm, self).__init__(*args, **kwargs)

		## Condiciones que evaluan si se ha especificado información en los campos,
		#  en caso afirmativo establece los mismos como requeridos
		if self.data:
			if self.data['tipo_vehiculo']:
				self.fields['placa_vehiculo'].required = True
				self.fields['marca_vehiculo'].required = True
				self.fields['modelo_vehiculo'].required = True
				self.fields['anho_vehiculo'].required = True
				self.fields['color_vehiculo'].required = True
				self.fields['observaciones_vehiculo'].required = True


class EstructuraForm(forms.Form):
	tipo_estructura = forms.ChoiceField(label=_(u"Tipo"), choices=[('','Seleccione...')]+TIPO_ESTRUCTURA, widget=forms.Select(attrs={
		'class': 'select2 form-control span3', 'title': _(u'Seleccione el el tipo de vehículo')
	}), required=False)

	observaciones_estructura = forms.CharField(label=_(u"Observaciones"), widget=forms.Textarea(attrs={
		'class': 'form-control widget span6', 'rows': '3'
	}), required=False)

	def __init__(self, *args, **kwargs):
		super(EstructuraForm, self).__init__(*args, **kwargs)

		## Condiciones que evaluan si se ha especificado información en los campos,
		#  en caso afirmativo establece los mismos como requeridos
		if self.data:
			if self.data['tipo_estructura']:
				self.fields['observaciones_estructura'].required = True


class NovedadForm(UnidadForm, ComisionForm, PersonaForm, VehiculoForm, EstructuraForm):
	fecha = forms.DateField(label=_(u"Fecha"), input_formats=["%d-%m-%Y",], widget=forms.TextInput(attrs={
        'class': 'span2', 'onkeypress': 'return validar_numeros(event);', 'maxlength': '10'
    }))
	
	hora_salida = forms.CharField(label=_(u"Hora de Salida"), widget=forms.TextInput(attrs={
        'class': 'span2', 'onkeypress': 'return validar_horas(event);', 'maxlength': '5'
    }))

	hora_llegada = forms.CharField(label=_(u"Hora de Llegada"), widget=forms.TextInput(attrs={
        'class': 'span2', 'onkeypress': 'return validar_horas(event);', 'maxlength': '5'
    }))

	hora_reporte = forms.CharField(label=_(u"Hora del Reporte"), widget=forms.TextInput(attrs={
        'class': 'span2', 'onkeypress': 'return validar_horas(event);', 'maxlength': '5'
    }))

	zona = forms.ModelChoiceField(
        label=_(u"Zona"), queryset=Zona.objects.all(), empty_label=_(u"Seleccione..."),
        widget=forms.Select(attrs={
            'class': 'select2 span3',
            'title': _(u"Seleccione la zona donde ocurrió la novedad"),
            'onchange': "actualizar_combo('/comun/ajax/actualizar_combo/', this.value, 'id_municipio', 'comun', 'Municipio', " \
            	"'zona', 'id', 'nombre', 'default')"
        })
    )
	
	municipio = forms.ModelChoiceField(
        label=_(u"Municipio"), queryset=Municipio.objects.all(), empty_label=_(u"Seleccione..."),
        widget=forms.Select(attrs={
            'class': 'select2 span3',
            'title': _(u"Seleccione el Municipio donde ocurrió la novedad"),
            'onchange': "actualizar_combo('/comun/ajax/actualizar_combo/', this.value, 'id_parroquia', 'comun', 'Parroquia', " \
            	"'municipio', 'id', 'nombre', 'default')"
        })
    )

	parroquia = forms.ModelChoiceField(
        label=_(u"Parroquia"), queryset=Parroquia.objects.all(), empty_label=_(u"Seleccione..."),
        widget=forms.Select(attrs={
            'class': 'select2 span3',
            'title': _(u"Seleccione la parroquia donde ocurrió la novedad"),
            'onchange': "actualizar_combo('/comun/ajax/actualizar_combo/', this.value, 'id_estacion', 'comun', 'Estacion', " \
            	"'parroquia', 'id', 'nombre', 'default')"
        })
    )

	estacion = forms.ModelChoiceField(
        label=_(u"Estación"), queryset=Estacion.objects.all(), empty_label=_(u"Seleccione..."),
        widget=forms.Select(attrs={
            'class': 'select2 span3',
            'title': _(u"Seleccione la estación que atendió la novedad")
        })
    )

	direccion = forms.CharField(label=_(u"Dirección"), widget=forms.Textarea(attrs={
		'class': 'span6', 'rows': '3'
	}))

	tipo_novedad = forms.ChoiceField(label=_(u"Tipo de Novedad"), choices=TIPO_NOVEDAD, widget=forms.RadioSelect(attrs={
		'class': 'radio', 'title': _(u"Indique el tipo de novedad")
	}))

	division = forms.ChoiceField(label=_(u"División"), choices=[('','Seleccione...')]+DIVISION, widget=forms.Select(attrs={
		'class': 'select2 form-control span4', 'title': _(u'Seleccione la división'),
		'onchange': "actualizar_combo('/comun/ajax/actualizar_combo/', this.value, 'id_tipo_procedimiento', 'comun', 'Procedimiento', " \
					"'division', 'id', 'nombre', 'default')"
	}))

	tipo_procedimiento = forms.ModelChoiceField(label=_(u"Tipo de Procedimiento"), queryset=Procedimiento.objects.all(), 
		empty_label=_(u"Seleccione..."), widget=forms.Select(attrs={
			'class': 'select2 form-control span5', 'title': _(u'Seleccione el tipo de procedimiento realizado'), 
			'onchange': 'return validar_formulario_novedad(this.value);'
		}))

	detalle_procedimiento = forms.CharField(label=_(u"Descripción"), widget=forms.Textarea(attrs={
		'class': 'form-control widget span6', 'rows': '3'
	}))

	def __init__(self, *args, **kwargs):
		super(NovedadForm, self).__init__(*args, **kwargs)

	def clean_hora_salida(self):
		hora_salida = self.cleaned_data['hora_salida']
		hora_llegada = self.data['hora_llegada']
		hora_reporte = self.data['hora_reporte']

		if hora_salida > hora_llegada:
			raise forms.ValidationError(_(u"Hora de salida mayor a hora de llegada"))
		elif hora_salida > hora_reporte:
			raise forms.ValidationError(_(u"Hora de salida mayor a hora de reporte"))

		return hora_salida

	def clean_hora_llegada(self):
		hora_salida = self.data['hora_salida']
		hora_llegada = self.cleaned_data['hora_llegada']
		hora_reporte = self.data['hora_reporte']

		if hora_llegada > hora_reporte:
			raise forms.ValidationError(_(u"Hora de llegada Mayor a hora de reporte"))
		elif hora_llegada < hora_salida:
			raise forms.ValidationError(_(u"Hora de llegada menor a hora de salida"))

		return hora_llegada

	def clean_hora_reporte(self):
		hora_salida = self.data['hora_salida']
		hora_llegada = self.data['hora_llegada']
		hora_reporte = self.cleaned_data['hora_reporte']

		if hora_reporte < hora_salida:
			raise forms.ValidationError(_(u"Hora de reporte menor a hora de salida"))
		elif hora_reporte < hora_llegada:
			raise forms.ValidationError(_(u"Hora de reporte menor a hora de llegada"))

		return hora_reporte


class ReporteForm(forms.Form):
	anho = forms.ChoiceField(label=_(u"Año"), choices=cargarAnhosNovedades(), widget=forms.Select(attrs={
		'class': 'select2 form-control span2', 'title': _(u"Seleccione el año de consulta")
	}))

	mes = forms.ChoiceField(label=_(u"Mes"), choices=cargarMesesNovedades(), widget=forms.Select(attrs={
		'class': 'select2 form-control span2', 'title': _(u"Seleccione el mes de consulta")
	}), required=False)

	tipo_reporte = forms.ChoiceField(label=_(u"Tipo de Gráfico"), choices=TIPO_GRAFICO_REPORTE, widget=forms.Select(attrs={
		'class': 'select2 form-control span2', 'title': _(u"Seleccione el tipo de gráfico de consulta")
	}))


class FiltroListForm(forms.Form):
	anho = forms.ChoiceField(label=_(u"Año"), choices=cargarAnhosNovedades(), widget=forms.Select(attrs={
		'class': 'select2 span2', 'title': _(u"Seleccione el año de consulta")
	}), required=False)

	mes = forms.ChoiceField(label=_(u"Mes"), choices=cargarMesesNovedades(), widget=forms.Select(attrs={
		'class': 'select2 span2', 'title': _(u"Seleccione el mes de consulta")
	}), required=False)

	tipo_novedad = forms.ChoiceField(label=_(u"Tipo de Novedad"), choices=[('','Seleccione...')]+TIPO_NOVEDAD, widget=forms.Select(attrs={
		'class': 'select2 span2', 'title': _(u"Seleccione el tipo de novedad de consulta")
	}), required=False)