# coding=utf-8
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from novedades.apps.novedad.models import Persona
from novedades.apps.comun.constantes import MSG_NOT_AJAX
import json

@login_required()
def buscar_persona(request):
	try:
		if not request.is_ajax():
			return HttpResponse(json.dumps({'resultado': False, 'error': MSG_NOT_AJAX}))

		cedula = request.GET.get('cedula', None)

		if cedula and Persona.objects.filter(cedula=cedula):
			persona = Persona.objects.get(cedula=cedula)
			return HttpResponse(json.dumps({'resultado': True, 'nombre': persona.nombre, 'edad': persona.edad, 'sexo': persona.sexo}))
		else:
			return HttpResponse(json.dumps({'resultado': False, 'error': 'No se ha especificado el registro'}))

	except Exception, e:
		print e
		return HttpResponse(json.dumps({'resultado': False, 'error': e}))