# -*- coding: utf-8 -*-
from django.db import models

from novedades.apps.comun.models import Procedimiento, Comision, Unidad, Diagnostico, Estacion
from novedades.apps.comun.constantes import TIPO_ESTRUCTURA, SEXO, ROL_PERSONA_VEHICULO, TIPO_NOVEDAD, PERSONA_ESTATUS,TIPO_VEHICULO

# Create your models here.

class Novedad(models.Model):
    fecha = models.DateField()
    hora_salida = models.TimeField()
    hora_llegada = models.TimeField()
    hora_reporte = models.TimeField()
    direccion = models.CharField(max_length=255)
    descripcion = models.CharField(max_length=255, null=True)
    tipo = models.CharField(max_length=2, choices=TIPO_NOVEDAD)
    estacion = models.ForeignKey(Estacion)
    procedimiento = models.ForeignKey(Procedimiento)


class Vehiculo(models.Model):
    tipo = models.CharField(max_length=2, choices= TIPO_VEHICULO)
    marca = models.CharField(max_length=50)
    modelo = models.CharField(max_length=50)
    anno = models.CharField(max_length=5)
    placa = models.CharField(max_length=10)
    color = models.CharField(max_length=15)


class Persona(models.Model):
    nombre = models.CharField(max_length=60, null=True)
    cedula = models.CharField(max_length=12, unique=True, null=True)
    edad = models.SmallIntegerField(null=True)
    sexo = models.CharField(max_length=1, null=True, choices=SEXO[1:])
    identificado = models.BooleanField()


class Estructura(models.Model):
    novedad = models.ForeignKey(Novedad)
    tipo = models.CharField(max_length=2, choices=TIPO_ESTRUCTURA)
    observacion = models.CharField(max_length=255, null=True)


class NovedadUnidad(models.Model):
    novedad = models.ForeignKey(Novedad)
    unidad = models.ForeignKey(Unidad)
    responsable = models.CharField(max_length=60, null=True)


class NovedadComision(models.Model):
    novedad = models.ForeignKey(Novedad)
    comision = models.ForeignKey(Comision)
    responsable = models.CharField(max_length=60, null=True)


class NovedadPersona(models.Model):
    novedad = models.ForeignKey(Novedad)
    persona = models.ForeignKey(Persona)
    observacion = models.CharField(max_length=255, null=True)


class NovedadVehiculo(models.Model):
    novedad = models.ForeignKey(Novedad)
    vehiculo = models.ForeignKey(Vehiculo)
    observacion = models.CharField(max_length=255, null=True)


class PersonaVehiculo(models.Model):
    persona = models.ForeignKey(Persona)
    vehiculo = models.ForeignKey(Vehiculo)
    tipo_persona = models.CharField(max_length=1, choices=ROL_PERSONA_VEHICULO, null=True)


class PersonaEstructura(models.Model):
    persona = models.ForeignKey(Persona)
    estructura = models.ForeignKey(Estructura)
    propietario = models.BooleanField(default=False)


class PersonaDiagnostico(models.Model):
    persona = models.ForeignKey(Persona)
    diagnostico = models.ForeignKey(Diagnostico)
    estatus = models.CharField(max_length=1, choices=PERSONA_ESTATUS)