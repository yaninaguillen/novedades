# coding=utf-8
from novedades.apps.novedad.models import Novedad, NovedadUnidad, NovedadComision, Persona, NovedadPersona, PersonaDiagnostico, \
Vehiculo, NovedadVehiculo, Estructura, PersonaEstructura
from novedades.apps.comun.models import Unidad, Diagnostico, Estacion, Procedimiento, Comision
from novedades.apps.comun.constantes import TIPO_NOVEDAD, SEXO, PERSONA_ESTATUS, TIPO_VEHICULO, TIPO_ESTRUCTURA

import random
import string

def cargar_datos(cantidad):
	anhos = [str(m) if len(str(m))>1 else "0"+str(m) for m in range(2010,2016)]
	meses = [str(m) if len(str(m))>1 else "0"+str(m) for m in range(1,13)]
	dias = [str(m) if len(str(m))>1 else "0"+str(m) for m in range(1,29)]
	horas = [str(m) if len(str(m))>1 else "0"+str(m) for m in range(0,24)]
	minutos = [str(m) if len(str(m))>1 else "0"+str(m) for m in range(0,60)]
	texto = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,"

	print "Iniciando registro de datos...\n\n"

	try:
		for x in range(cantidad):
			print "Registrando datos para el elemento [%s]\n\n" % str(x+1)
			fecha = "%s-%s-%s" % (random.choice(anhos), random.choice(meses), random.choice(dias))
			hora_salida = "%s:%s" % (random.choice(horas), random.choice(minutos))
			hora_llegada = "%s:%s" % (random.choice(horas), random.choice(minutos))
			hora_reporte = "%s:%s" % (random.choice(horas), random.choice(minutos))
			estacion = random.choice(Estacion.objects.filter(unidad__isnull=False))
			direccion = texto[:255]
			tipo_novedad = random.choice(TIPO_NOVEDAD)[0]
			tipo_procedimiento = random.choice(Procedimiento.objects.all())
			detalle_procedimiento = texto[:255]

			# Registro aleatorio de novedades
			novedad = Novedad.objects.create(
				fecha=fecha, hora_salida=hora_salida, hora_llegada=hora_llegada, hora_reporte=hora_reporte,
				estacion=estacion, direccion=direccion, tipo=tipo_novedad, descripcion=detalle_procedimiento, 
				procedimiento=tipo_procedimiento
			)

			# Registro aleatorio de Unidades
			codigo_unidad = random.choice(Unidad.objects.filter(estacion=estacion))
			responsable_unidad = texto[:60]
			novedad_unidad = NovedadUnidad.objects.create(unidad=codigo_unidad, responsable=responsable_unidad, novedad=novedad)

			# Registro aleatorio de Comisiones
			nombre_comision = random.choice(Comision.objects.all())
			responsable_comision = texto[:60]

			novedad_comision = NovedadComision.objects.create(
				comision=nombre_comision, responsable=responsable_comision, novedad=novedad
			)

			# Registro aleatorio de Personas
			identificado = random.choice([True, False])
			persona_nombre = texto[:60]
			persona_cedula = ''.join(random.choice([random.choice(string.digits)]) for x in range(10))
			persona_edad = ''.join(random.choice([random.choice(string.digits)]) for x in range(2))
			persona_sexo = random.choice(SEXO[1:])[0]

			persona = Persona.objects.create(
				nombre=persona_nombre, cedula=persona_cedula, edad=persona_edad, sexo=persona_sexo,
				identificado=identificado
			)

			persona_observaciones = texto[:255]
			novedad_persona = NovedadPersona.objects.create(
				observacion=persona_observaciones, persona=persona, novedad=novedad
			)

			diagnostico=random.choice(Diagnostico.objects.all())
			persona_estatus=random.choice(PERSONA_ESTATUS)[0]

			persona_diagnostico = PersonaDiagnostico.objects.create(
				estatus=persona_estatus, diagnostico=diagnostico, persona=persona
			)

			# Registro aleatorio de Vehículos
			tipo_vehiculo = random.choice(TIPO_VEHICULO)[0]
			marca_vehiculo = texto[:50]
			modelo_vehiculo = texto[:50]
			anho_vehiculo = random.choice(anhos)
			placa_vehiculo = texto[:10]
			color_vehiculo = texto[:15]
			observaciones_vehiculo = texto[:255]

			vehiculo = Vehiculo.objects.create(
				tipo=tipo_vehiculo, marca=marca_vehiculo, modelo=modelo_vehiculo,
				anno=anho_vehiculo, placa=placa_vehiculo, color=color_vehiculo
			)

			novedad_vehiculo = NovedadVehiculo.objects.create(
				novedad=novedad, vehiculo=vehiculo, observacion=observaciones_vehiculo
			)

			# Registro aleatorio de Estructuras
			tipo_estructura = random.choice(TIPO_ESTRUCTURA)[0]
			observaciones_estructura = texto[:255]
			estructura = Estructura.objects.create(
				novedad=novedad, tipo=tipo_estructura, observacion=observaciones_estructura
			)

			if persona:
				persona_estructura = PersonaEstructura.objects.create(
					persona=persona, estructura=estructura
				)

			print "Se registraron los datos para el elemento [%s]\n\n" % str(x+1)

		print "Se han registrado %s novedades" % str(cantidad)
	except Exception, e:
		print "Error cargando datos: %s" % e