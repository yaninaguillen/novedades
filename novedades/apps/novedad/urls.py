from django.conf.urls import url, patterns
from django.contrib.auth.decorators import login_required
from novedades.apps.novedad import views

urlpatterns = [
    url(r'^novedades/$', login_required(views.NovedadCreate.as_view()), name='novedades'),
    url(r'^consulta_novedad/$', 'novedades.apps.novedad.views.consultar_novedades', name='consulta_novedad'),
    url(r'^update_novedad/(?P<pk>\d+)/$', login_required(views.NovedadUpdate.as_view()), name='modificar_novedad'),
    url(r'^reporte_general/$', 'novedades.apps.novedad.views.reporte_general', name='reporte_general'),
    url(r'^ajax/buscar_persona/$', 'novedades.apps.novedad.ajax.buscar_persona', name="buscar_persona"),
]

"""url(r'^emergencia_prehospitalario/$', login_required(views.NovedadEmergenciaPrehospitalariaCreate.as_view()), name='novedad_emergencia_prehospitalaria'),
    url(r'^busqueda_rescate/$', login_required(views.NovedadBusquedaRescateCreate.as_view()), name='novedad_busqueda_rescate'),"""