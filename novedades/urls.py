#from django.conf.urls import patterns, include, url

#from django.contrib import admin
#admin.autodiscover()

from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import RedirectView
from django.contrib.auth.decorators import login_required
from novedades.apps.usuario import views

#from model_report import report
import os

admin.autodiscover()
#report.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'novedades.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'novedades.apps.comun.views.inicio', name='inicio'),
    (r'^usuario/', include('novedades.apps.usuario.urls')),
    (r'^comun/', include('novedades.apps.comun.urls')),
    (r'^novedad/', include('novedades.apps.novedad.urls')),
    #url(r'^comun/', include('novedades.apps.comun.urls')),
    #url(r'^novedad/', include('novedades.apps.novedad.urls')),
    url(r'^login/?$', 'novedades.apps.usuario.views.acceso', name='acceso'),
    url(r'^logout/?$', 'novedades.apps.usuario.views.salir', name='salir'),
    url(r'^forgot/$', views.OlvidoPassView.as_view(), name='forgot_password'),
)
